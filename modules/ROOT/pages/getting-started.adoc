= Getting Started with Fedora/CentOS bootc

== Introduction

The Fedora/CentOS bootc project generates reference "base images" that
are designed for use with the https://github.com/containers/bootc[bootc project].

=== Container Images

These are the two "full" container images:

- {container-c9s} (CentOS Stream 9)
- {container-fedora-full} (Fedora 40)

=== Provisioning Philosophy

It is generally expected that you create a *custom* container image, derived from one of our base images for deployment.

For more information on configuration, refer to the documentation for xref:building-containers.adoc[Building containers].

== Quickstart

=== Inspecting as a container

include::getting-started-container.adoc[]

However, it is more interesting to make a custom derived container image, and
run it.  See xref:building-containers.adoc[Building derived container images].

=== Booting on a local hypervisor

include::getting-started-podman-bootc-cli.adoc[Using podman-bootc-cli]

=== Using Podman Desktop with the bootc extension

See https://github.com/containers/podman-desktop-extension-bootc[Podman Desktop bootc].

=== Exploring the OS for the first time

By design, there are no hard-coded default credentials or non-root interactive users.

If you set up an xref:authentication.adoc[SSH key], you can SSH into the VM and explore the OS:

[source, bash]
----
ssh root@<ip address>
----

Note that the `podman-bootc` CLI by default automatically invokes `ssh` in this way.

=== Key related projects

- https://github.com/containers/bootc[bootc]
- https://github.com/osbuild/bootc-image-builder[bootc-image-builder]
- https://gitlab.com/bootc-org/podman-bootc-cli[podman-bootc-cli]
- https://github.com/containers/podman-desktop-extension-bootc[podman-desktop-extension-bootc]

Other projects:

- https://github.com/cgwalters/osbuild-cfg

Established key related and included projects:

- https://github.com/containers/podman[podman]
- https://systemd.io[systemd]

== Getting in touch

Bugs can be reported to:

- Fedora: <https://gitlab.com/bootc-org/fedora-bootc/fedora-bootc-base>
- CentOS: <https://github.com/CentOS/centos-bootc/>

For longer discussions related to Fedora bootc, use the https://discussion.fedoraproject.org/tag/bootc[forum].